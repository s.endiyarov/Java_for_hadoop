package Tests;

import Utils.HadoopFiles;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.json.simple.JSONObject;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

public class SequenceFileTestOps {

    private final static String host = "hdfs://localhost:8020/";
    private final static String dfs_replication = "1";
    private final static String hadoop_config = "hadoop-conf.xml";
    private final FileSystem fs;

    private  Configuration conf;


    private final Cache<String, Path> Paths = CacheBuilder.newBuilder()
            .expireAfterWrite(60, TimeUnit.MINUTES)
            .build();


    public SequenceFileTestOps() throws IOException {
        Utils.ConfigReader configReader = new Utils.ConfigReader();
        conf = new Configuration();
        conf.addResource(configReader.getFileFromResourceAsStream(hadoop_config));
        conf.set("dfs.replication", dfs_replication);
        fs = FileSystem.get(conf);
    }

    public OperationState<SequenceFileOperation> deleteSequenceFile(SequenceFileOperation operation){
        Path path = getPath(host+operation.getFileName());

        try {
            boolean result = this.fs.delete(path, false);
            return  new OperationState<>(result, !result? new Exception("Err"):null, operation );
        } catch (IOException e) {
           return new OperationState<>(false, e, operation);
        }
    }

    public OperationState<SequenceFileOperation> readSequenceFileKey(SequenceFileOperation operation){
        byte[] data;
        try {
            Path file = getPath(host+operation.getFileName());
            Path fileMap = getPath(host+getMapName(operation.getFileName()));
            data = HadoopFiles.readSequenceFileByKey(this.conf, file, fileMap, operation.getKey());
        } catch (IOException e) {
            return new OperationState<>(false, e, operation);
        }
        return new OperationState<>(true, null, operation.setFileContent(data));
    }

    public OperationState<SequenceFileOperation> writeToSequenceFile(SequenceFileOperation operation, InputStream stream, boolean isAppend){
        try {
            Path file = getPath(host+operation.getFileName());
            Path fileMap = getPath(host+getMapName(operation.getFileName()));
            byte[] data = stream.readAllBytes();
            boolean result = Utils.HadoopFiles.writeToSequenceFileByKey(conf, file, fileMap, operation.getKey(), data, isAppend);
            return new OperationState<>(result, !result?new Exception("Write to file error!"):null, operation);
        } catch (IOException e) {
            e.printStackTrace();
            return new OperationState<>(false, e, operation);
        }
    }

    private String getMapName(String filename) throws IOException {
        String mapName = filename.replace(".seq", ".map");
        if (mapName.equals(filename)) {
            throw new IOException("Sequence file name must have '.seq' extension");
        }
        return mapName;
    }

    private synchronized Path getPath(String filename) {
        Path path = Paths.getIfPresent(filename);
        if (path == null) {
            path = new Path(filename);
            Paths.put(filename, path);
        }
        return path;
    }

}
