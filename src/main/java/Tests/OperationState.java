package Tests;

public class OperationState<T> {
    boolean isSuccess;
    Exception error;
    T data;

    public OperationState(boolean isSuccess, Exception error, T data) {
        this.isSuccess = isSuccess;
        this.error = error;
        this.data = data;
    }

    public Exception getError() {
        return error;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public T getData() {
        return data;
    }
}
