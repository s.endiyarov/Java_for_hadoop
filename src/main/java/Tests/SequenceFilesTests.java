package Tests;

import Utils.LocalFiles;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SequenceFilesTests {


    public static List<String> files = new ArrayList<>() {{
        this.add("src\\main\\resources\\test_to_hadoop1.mp4");
        this.add("src\\main\\resources\\test_to_hadoop2.mp4");
        this.add("src\\main\\resources\\test_to_hadoop3.mp4");
        this.add("src\\main\\resources\\test_to_hadoop4.mp4");
    }};

    public static String getRandomFileName() {
        Random random = new Random();
        int index = random.nextInt(files.size());
        return files.get(index);
    }

    public static String generateKey() {
        Random random = new Random();
        int index = random.nextInt(files.size());
        return "key:" + index + "_v:" + String.valueOf(random.nextDouble());
    }


    static class WriteSequenceFilePart implements Runnable {
        private final SequenceFileTestOps ops;
        private final SequenceFileOperation fileOperation;
        private final InputStream data;

        public WriteSequenceFilePart(SequenceFileTestOps ops, SequenceFileOperation fileOperation, InputStream data) {
            this.ops = ops;
            this.fileOperation = fileOperation;
            this.data = data;
        }

        @Override
        public void run() {

            System.out.println("Executing by:" + Thread.currentThread().getName());

            System.out.println("Executing write of " + fileOperation.getFileName() + "key:" + fileOperation.getKey());
            this.ops.writeToSequenceFile(fileOperation, data, true);
        }
    }

    public static void main(String[] args) throws Exception {

        final int MAX_FILES = 115;
        final String SEQUENCE_FILE_NAME = "mySequenceFile.seq";

        List<InputStream> inputStreams = new ArrayList<>();
        List<String> keys = new ArrayList<>();
        List<String> fileNames = new ArrayList<>();
        //write 15 files to SF async
        SequenceFileTestOps sfFile = new SequenceFileTestOps();

        OperationState<SequenceFileOperation> opState = sfFile.deleteSequenceFile(new SequenceFileOperation(SEQUENCE_FILE_NAME, null));
        assert opState.isSuccess;

        List<Runnable> jobs = new ArrayList<>();

        for (int i = 1; i <= MAX_FILES; i++) {
            String randomFileName = getRandomFileName();
            String generateKey = generateKey();
            System.out.println(randomFileName);
            System.out.println(generateKey);
            fileNames.add(randomFileName);
            inputStreams.add(LocalFiles.readInputStream(randomFileName));
            keys.add(generateKey);
            jobs.add(new WriteSequenceFilePart(sfFile, new SequenceFileOperation(SEQUENCE_FILE_NAME, generateKey), inputStreams.get(inputStreams.size() - 1)));
        }
        int jobId = 0;
        ExecutorService executor = Executors.newFixedThreadPool(16);
        for (Runnable job : jobs) {
            jobId++;
            System.out.println("Job started ! " + jobId);
            executor.execute(job);
        }
        executor.shutdown();

        try {
            executor.awaitTermination(5, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("All done ! ");

        // locate all keys in map
        int index = 0;
        for (String key : keys) {
            OperationState<SequenceFileOperation> readState = sfFile.readSequenceFileKey(new SequenceFileOperation(SEQUENCE_FILE_NAME, key));
            assert readState.isSuccess;

            byte[] srcFile = LocalFiles.readInputStream(fileNames.get(index)).readAllBytes();
            byte[] hadoopFile = readState.getData().getFileContent();

            boolean isEqual = Arrays.equals(srcFile, hadoopFile);

            if(!isEqual) {
                throw new Exception("Failed!");
            }else{
                System.out.println(String.format("File with key %s and name %s are equals!", key, fileNames.get(index)));
            }

            index++;

        }


        //find all files and compare

        //read async and compare

        //simualteneous write/read?

    }
}
