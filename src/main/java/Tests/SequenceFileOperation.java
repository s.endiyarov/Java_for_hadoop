package Tests;

public class SequenceFileOperation {
    private final String fileName;
    private final String key;
    private byte []  fileContent;

    public SequenceFileOperation(String fileName, String key) {
        this.fileName = fileName;
        this.key = key;
    }

    public String getFileName() {
        return fileName;
    }

    public String getKey() {
        return key;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public SequenceFileOperation setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
        return this;
    }
}
