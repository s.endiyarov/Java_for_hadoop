package RestService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import Utils.HadoopFiles;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;

import org.json.simple.JSONObject;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import RestService.Models.Greeting;

@RestController
public class Controller {
	private final static String host = "hdfs://localhost:8020/";
	private final static String dfs_replication = "1";
	private final static String hadoop_config = "hadoop-conf.xml";

	private final FileSystem fs;
	private final Configuration conf;

//	private final Map<String, Path> Paths = new ConcurrentHashMap<>();
	private final Cache<String, Path> Paths = CacheBuilder.newBuilder()
       .expireAfterWrite(60, TimeUnit.MINUTES)
       .build();

	private Controller() throws IOException {
		Utils.ConfigReader configReader = new Utils.ConfigReader();
		conf = new Configuration();
//		conf.set("io.serializations", "org.apache.hadoop.io.serializer.JavaSerialization");
		conf.addResource(configReader.getFileFromResourceAsStream(hadoop_config));
		conf.set("dfs.replication", dfs_replication);
		fs = FileSystem.get(conf);
	}

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(name = "name", defaultValue = "World") String name) {
		String template = "Hello, %s!";
		AtomicLong counter = new AtomicLong();
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	@GetMapping("/api/hadoop/v1/download")
	public ResponseEntity<Resource> download(
			@RequestParam(name = "filename", required = true) String filename
	) {
		Path file = getPath(host+filename);

		byte[] data;
		ByteArrayResource response;

		try {
			data = Utils.HadoopFiles.readFromSimpleFile(this.fs, file);
		} catch (IOException e) {
			response = new ByteArrayResource(e.getMessage().getBytes());
			return ResponseEntity.badRequest()
					.contentType(MediaType.TEXT_PLAIN)
					.body(response);
		}

		if (data == null) return ResponseEntity.notFound().build();

		response = new ByteArrayResource(data);

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+filename);
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		return ResponseEntity.ok()
				.headers(headers)
				.contentLength(data.length)
				.contentType(MediaType.APPLICATION_OCTET_STREAM)
				.body(response);
	}

	@GetMapping("/api/hadoop/v1/downloadSeq")
	public ResponseEntity<Resource> downloadSeq(
			@RequestParam(name = "filename", required = true) String filename,
			@RequestParam(name = "key", required = true) String key
	) {
		byte[] data;
		ByteArrayResource response;

		try {
			Path file = getPath(host+filename);
			Path fileMap = getPath(host+getMapName(filename));

			data = HadoopFiles.readSequenceFileByKey(this.conf, file, fileMap, key);
		} catch (IOException e) {
			response = new ByteArrayResource(e.getMessage().getBytes());
			return ResponseEntity.badRequest()
					.contentType(MediaType.TEXT_PLAIN)
					.body(response);
		}

		if (data.length == 0) {
			return ResponseEntity.notFound().build();
		}

		response = new ByteArrayResource(data);

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+filename+"_"+key);
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		return ResponseEntity.ok()
				.headers(headers)
				.contentLength(data.length)
				.contentType(MediaType.APPLICATION_OCTET_STREAM)
				.body(response);
	}

	@PostMapping("/api/hadoop/v1/upload")
	public ResponseEntity<JSONObject> upload(
			InputStream dataStream,
			@RequestParam(name = "filename", required = true) String filename
	) {
		byte[] data = new byte[0];

		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			data = dataStream.readAllBytes();
			Path file = getPath(host+filename);
			Utils.HadoopFiles.writeToSimpleFile(this.fs, file, data, false);
			result = true;
		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	@PostMapping("/api/hadoop/v1/uploadSeq")
	public ResponseEntity<JSONObject> uploadSeq(
			InputStream dataStream,
			@RequestParam(name = "filename", required = true) String filename,
			@RequestParam(name = "key", required = true) String key,
			@RequestParam(name = "isAppend", required = false, defaultValue = "true") String isAppend
	) {
		byte[] data = new byte[0];

		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			Path file = getPath(host+filename);
			Path fileMap = getPath(host+getMapName(filename));

			data = dataStream.readAllBytes();
			dataStream.close();

			result = Utils.HadoopFiles.writeToSequenceFileByKey(this.conf, file, fileMap, key, data, isAppend.equals("true"));
			if (!result) {
				message = "Key already exists";
				status = HttpStatus.ALREADY_REPORTED;
			}

		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	@PostMapping("/api/hadoop/v1/concat")
	public ResponseEntity<JSONObject> concat(
			@RequestParam(name = "to", required = true) String target,
			@RequestParam(name = "from", required = true) String files
	) {
		String[] fileNames = files.split(",", -1);
		String firstName = fileNames[0];
		fileNames = (String[]) ArrayUtils.remove(fileNames, 0);

		Path targetPath = getPath(host+target);
		Path firstPath = getPath(host+firstName);
		Path[] filesToConcat = Stream
				.of(fileNames)
				.map(filename -> getPath(host + filename))
				.toArray(Path[]::new);

		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			if (this.fs.exists(targetPath)) {
				message = "Target file already exists";
				status = HttpStatus.BAD_REQUEST;
			} else {
				this.fs.concat(firstPath, filesToConcat);
				result = this.fs.rename(firstPath, targetPath);
			}
		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	@PostMapping("/api/hadoop/v1/concatSeq")
	public ResponseEntity<JSONObject> concatSeq(
			@RequestParam(name = "to", required = true) String target,
			@RequestParam(name = "from", required = true) String files,
			@RequestParam(name = "key", required = true) String key,
			@RequestParam(name = "isAppend", required = false, defaultValue = "true") String isAppend
	) {
		String[] fileNames = files.split(",", -1);
		Path[] filesToConcat = Stream
				.of(fileNames)
				.map(filename -> getPath(host + filename))
				.toArray(Path[]::new);

		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			Path targetPath = getPath(host+target);
			Path targetMapPath = getPath(host+getMapName(target));

			// read files to stream
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			for (Path filePath: filesToConcat) {
				byte[] fileData = Utils.HadoopFiles.readFromSimpleFile(this.fs, filePath);
				if (fileData == null) throw new IOException("File "+filePath.getName()+" not exists");
				stream.write(fileData);
			}
			byte[] data = stream.toByteArray();

			// write stream data to sequence file
			result = Utils.HadoopFiles.writeToSequenceFileByKey(this.conf, targetPath, targetMapPath, key, data, isAppend.equals("true"));

			// delete files
			if (result) {
				for (Path filePath : filesToConcat) {
					this.fs.delete(filePath, false);
				}
			}
		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	@GetMapping(value = "/api/hadoop/v1/makeDir", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JSONObject> makeDir(
			@RequestParam(name = "name", required = true) String name
	) {
		Path path = getPath(host+name);

		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			result = this.fs.mkdirs(path);
		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	@GetMapping("/api/hadoop/v1/delete")
	public ResponseEntity<JSONObject> delete(
			@RequestParam(name = "name", required = true) String name
	) {
		Path path = getPath(host+name);

		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			result = this.fs.delete(path, false);
		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	@GetMapping("/api/hadoop/v1/rename")
	public ResponseEntity<JSONObject> rename(
			@RequestParam(name = "src", required = true) String src,
			@RequestParam(name = "dst", required = true) String dst
	) {
		Path pathSrc = getPath(host+src);
		Path pathDst = getPath(host+dst);

		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			result = this.fs.rename(pathSrc, pathDst);
		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	@GetMapping("/api/hadoop/v1/isExists")
	public ResponseEntity<JSONObject> isExists(
			@RequestParam(name = "name", required = true) String name
	) {
		Path path = getPath(host+name);

		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			result = this.fs.exists(path);
		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	@GetMapping("/api/hadoop/v1/isExistsSeq")
	public ResponseEntity<JSONObject> isExistsSeq(
			@RequestParam(name = "filename", required = true) String filename,
			@RequestParam(name = "key", required = true) String key
	) {
		JSONObject json = new JSONObject();
		boolean result = false;
		String message = "";
		HttpStatus status = HttpStatus.OK;

		try {
			Path fileMap = getPath(host+getMapName(filename));
			result = Utils.HadoopFiles.existsKeyInSequenceFile(this.conf, fileMap, key);
		} catch (IOException e) {
			message = e.getMessage();
			if (message == null) message = e.toString();
			status = HttpStatus.BAD_REQUEST;
		}

		json.put("boolean", result);
		if (!message.equals("")) json.put("message", message);

		return new ResponseEntity<>(json, status);
	}

	private static String getMapName(String filename) throws IOException {
		String mapName = filename.replace(".seq", ".map");
		if (mapName == filename) {
			throw new IOException("Sequence file name must have '.seq' extension");
		}
		return mapName;
	}

	private synchronized Path getPath(String filename) {
//		Path path = Paths.get(filename);
		Path path = Paths.getIfPresent(filename);
		if (path == null) {
			path = new Path(filename);
			Paths.put(filename, path);
		}
		return path;
	}
}
