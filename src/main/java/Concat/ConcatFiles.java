package Concat;

import Utils.ConfigReader;
import com.google.common.primitives.Bytes;
import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConcatFiles {

    public static void createFile(FileSystem fs,
                                  Path path,
                                  boolean overwrite,
                                  byte[] data) throws IOException {
        FSDataOutputStream stream = fs.create(path, overwrite);
        if (data != null && data.length > 0) {
            stream.write(data);
        }
        stream.close();
    }

    public static void main(String[] args) throws IOException {
        ConfigReader configReader = new ConfigReader();

        Configuration conf = new Configuration();
        conf.set("io.serializations",
                "org.apache.hadoop.io.serializer.JavaSerialization");
        conf.addResource(configReader.getFileFromResourceAsStream("hadoop-conf.xml"));
        conf.set("dfs.replication", "1");

        FileSystem fs = FileSystem.get(conf);

//        InputStream stream = configReader.getFileFromResourceAsStream("test_to_hadoop.txt");
//        byte[]  fileContent = stream.readAllBytes();

        List<Path> filesToConcat = new ArrayList<>();

        for(int i=0;i<=100;i++) {
            Path filePath = new Path("hdfs://localhost:8020/test2/src" + i + ".txt");
            filesToConcat.add(filePath);
        }

        Path[] lst = filesToConcat.toArray(new Path[0]);
//        ArrayUtils.reverse(lst);

        Collections.shuffle(filesToConcat);

        for (Path p: filesToConcat
        ) {
            System.out.println(p.getName());
            createFile(fs, p, true, String.valueOf(p.getName()+"\n").getBytes(StandardCharsets.UTF_8));
        }

        Path filePath = new Path("hdfs://localhost:8020/test2/destFile.txt");
        createFile(fs, filePath, true, new byte[0]);

        for (Path p: lst
             ) {
            System.out.println(p);
        }

        fs.concat(new Path("hdfs://localhost:8020/test2/destFile.txt"), lst);
    }
}
