package Utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.util.ReflectionUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HadoopFiles {
    public static void writeToSimpleFile(org.apache.hadoop.fs.FileSystem fs, org.apache.hadoop.fs.Path file, byte[] data, boolean isAppend) throws IOException {
        FSDataOutputStream stream;
        if (isAppend && fs.exists(file)) {
            stream = fs.append(file);
        } else {
            stream = fs.create(file);
        }
        stream.write(data);
        stream.close();
    }

    public static byte[] readFromSimpleFile(org.apache.hadoop.fs.FileSystem fs, Path file) throws IOException {
        byte[] data = null;
        if (fs.exists(file)) {
            FSDataInputStream stream = fs.open(file);
            data = stream.readAllBytes();
            stream.close();
        }
        return data;
    }

    public static boolean existsKeyInSequenceFile(Configuration conf, Path mapFile, String key) throws IOException {
//        System.out.printf("Checking existence of value WITH map [key %s]\n", key);

        Map<String, Long> map = readMapFile(conf, mapFile);
        if (map == null) return false;

        Long position = map.get(key);
        return position != null;
    }

    /*
    public static void writeArrayToSequenceFile(Configuration conf, org.apache.hadoop.fs.Path file, Path mapFile, DataItem[] data, boolean isAppend) throws IOException {
        Map<String, Long> map = new HashMap<>();

        Text myKey = new Text();
        BytesWritable myValue = new BytesWritable();

        SequenceFile.Writer writer = SequenceFile.createWriter(
                conf,
                SequenceFile.Writer.file(file),
                SequenceFile.Writer.keyClass(myKey.getClass()),
                SequenceFile.Writer.valueClass(myValue.getClass()),
                SequenceFile.Writer.appendIfExists(isAppend));

//        System.out.printf("Write to hadoop data, length: %s\n",data.length);

        try {
            for (DataItem item: data) {
//                System.out.printf("[write key: %s]\n", item.key);
                myKey.set(item.key);
                myValue.set(new BytesWritable(item.data));
                map.put(item.key, writer.getLength());
                writer.append(myKey, myValue);
            }
        } finally {
            IOUtils.closeStream(writer);
        }

        writeMapFile(conf, map, mapFile, isAppend);
    }
    */

    public static boolean writeToSequenceFileByKey(Configuration conf, org.apache.hadoop.fs.Path file, Path mapFile, String key, byte[] data, boolean isAppend) throws IOException {
//        System.out.printf("Writing single value WITH map [key %s]\n", key);
        if (isAppend && existsKeyInSequenceFile(conf, mapFile, key)) {
            throw new IOException("Key "+key+" already exists");
        }

        Map<String, Long> map = new HashMap<>();
        Text myKey = new Text();
        BytesWritable myValue = new BytesWritable();

        myKey.set(key);
        myValue.set(new BytesWritable(data));

        synchronized (file) {
            SequenceFile.Writer writer = SequenceFile.createWriter(
                    conf,
                    SequenceFile.Writer.file(file),
                    SequenceFile.Writer.keyClass(myKey.getClass()),
                    SequenceFile.Writer.valueClass(myValue.getClass()),
                    SequenceFile.Writer.appendIfExists(isAppend));
            map.put(key, writer.getLength());
            writer.append(myKey, myValue);
            writer.close();
        }

        writeMapFile(conf, map, mapFile, isAppend);

        return true;
    }

    public static byte[] readSequenceFileByKey(Configuration conf, Path file, Path mapFile, String key) throws IOException {
//        System.out.printf("Reading single value WITH map for [srcKey %s]\n", key);
        Map<String, Long> map = readMapFile(conf, mapFile);

        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(file)
        );

        Writable myKey = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
        BytesWritable myValue = (BytesWritable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

        try {
            Long position = map.get(key);
            if (position == null) {
                System.out.printf("readSequenceFileByKey: [srcKey=%s position=NOT FOUND]\n", key);
            } else {
                reader.seek(position);
                reader.next(myKey, myValue);
                if (myKey!=null && key.equals(myKey.toString())) {
//                System.out.printf("readSequenceFileByKey [srcKey=%s fileKey=%s position=%s]\n", key, myKey, position);
                } else {
                    System.out.printf("readSequenceFileByKey ERROR: [srcKey=%s fileKey=%s position=%s]\n", key, myKey, position);
                }
            }

        } finally {
            IOUtils.closeStream(reader);
        }

        return myValue.copyBytes();
    }

    private static String convertMapToString(Map<String, Long> nav) {
        StringBuilder str = new StringBuilder();
        nav.forEach((key, position) -> {
//            str.append(key+"="+position+"\n");
            str.append(key).append("=").append(position).append("\n");
        });
        return str.toString();
    }

    private static Map<String, Long> convertMapFromString(String str) {
        Map<String, Long> nav = new HashMap<>();
        String[] array = str.split("\n", -1);
        for (String item: array) {
            int pos = item.indexOf("=");
            if (pos >= 0) {
                String key = item.substring(0, pos);
                Long position = Long.parseLong(item.substring(pos+1));
                nav.put(key, position);
            }
        }
        return nav;
    }

    private static void writeMapFile(Configuration conf, Map<String, Long> nav, Path mapFile, boolean isAppend) throws IOException {
        FileSystem fs = FileSystem.get(conf);
        byte[] data = convertMapToString(nav).getBytes();
        synchronized (mapFile) {
            writeToSimpleFile(fs, mapFile, data, isAppend);
        }
    }

    private static Map<String, Long> readMapFile(Configuration conf, Path mapFile) throws IOException {
        FileSystem fs = FileSystem.get(conf);
        byte[] data = readFromSimpleFile(fs, mapFile);
        if (data == null) return null;
        String str = new String(data);
        return convertMapFromString(str);
    }
}
