package Utils;

import java.io.IOException;
import java.util.Date;

public class PrintRunTime {
    @FunctionalInterface
    public interface Func {
        void run() throws IOException;
    }

    public static void print(Func func) throws IOException {
        Date start = new Date();

        func.run();

        Date end = new Date();
        System.out.printf("from: %s\n", start.toInstant());
        System.out.printf("  to: %s\n", end.toInstant());
        System.out.printf("elapsed: %s\n", end.getTime()-start.getTime()+"\n");
    }
}
