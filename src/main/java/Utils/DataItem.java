package Utils;

public class DataItem {
    public String key;
    public byte[] data;

    public DataItem(String key, byte[] data) {
        this.key = key;
        this.data = data;
    }
}
