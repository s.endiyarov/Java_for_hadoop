package Utils;

import java.io.*;
import java.nio.file.*;

public class LocalFiles {
    public static void writeData(String filename, byte[] data) throws IOException {
        Files.write(Paths.get(filename), data);
    }
    public static byte[] readData(String filename) throws IOException {
        return Files.readAllBytes(Paths.get(filename));
    }

    public static InputStream readInputStream(String filename) throws IOException {
        return new FileInputStream(filename);
    }
}
