package Utils;

import org.apache.commons.lang.ArrayUtils;

import java.io.IOException;

public class TestData {
    public static DataItem[] getData() throws IOException {
        int count = 10;
        DataItem[] data = new DataItem[0];

        for (int i=1; i<=count; i++) {
            DataItem item = new DataItem(
                "id" + i,
                LocalFiles.readData("src\\main\\resources\\test_to_hadoop" + (i % 4 + 1) + ".mp4")
            );
            data = (DataItem[]) ArrayUtils.add(data, item);
        }

        return data;
    }
}
