package WorkingWithSequenceFiles;

import Utils.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.util.*;

public class SequenceFileBinary_WriteAndReadWithMap_Test2 {

    void testRun(Configuration conf, DataItem[] fileData, Path file, Path mapFile) throws IOException {
        boolean isLog = false;

        System.out.printf("Write to hadoop data, length: %s\n", fileData.length);
        PrintRunTime.print(() -> {
            boolean isAppend = false;
            for (DataItem item : fileData) {
                boolean result = HadoopFiles.writeToSequenceFileByKey(conf, file, mapFile, item.key, item.data, isAppend);
                if (!result) System.out.println("key already exists");
                isAppend = true;
            }
        });

        System.out.println("Check read WITH map - ordered");
        PrintRunTime.print(() -> {
            for (DataItem item : fileData) {
                byte[] data = HadoopFiles.readSequenceFileByKey(conf, file, mapFile, item.key);
                if (Arrays.equals(data, item.data)) {
                    if (isLog) System.out.printf("OK [check key: %s]\n", item.key);
                } else {
                    System.out.printf("ERROR [check key: %s]\n", item.key);
                }
            }
        });

        List<DataItem> targetList = Arrays.asList(fileData);
        Collections.shuffle(targetList);

        System.out.println("Check read WITH map - shuffled");
        PrintRunTime.print(() -> {
            for (DataItem item : fileData) {
                byte[] data = HadoopFiles.readSequenceFileByKey(conf, file, mapFile, item.key);
                if (Arrays.equals(data, item.data)) {
                    if (isLog) System.out.printf("OK [check key: %s]\n", item.key);
                } else {
                    System.out.printf("ERROR [check key: %s]\n", item.key);
                }
            }
        });
    }

    public static void main(String[] args) throws IOException {
        ConfigReader configReader = new ConfigReader();
        Configuration conf = new Configuration();
        conf.addResource(configReader.getFileFromResourceAsStream("hadoop-conf.xml"));
        conf.set("dfs.replication", "1");

        DataItem[] fileData = TestData.getData();

        Path outputFile = new Path("hdfs://localhost:8020/test/seqFileSeekHuge.seq");
        Path mapFile = new Path("hdfs://localhost:8020/test/seqFileSeekHuge.map");

        SequenceFileBinary_WriteAndReadWithMap_Test2 s = new SequenceFileBinary_WriteAndReadWithMap_Test2();
        s.testRun(conf, fileData, outputFile, mapFile);
    }

}
