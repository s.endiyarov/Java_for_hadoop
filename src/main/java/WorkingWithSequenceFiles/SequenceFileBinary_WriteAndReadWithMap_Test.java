package WorkingWithSequenceFiles;

import Utils.ConfigReader;
import Utils.DataItem;
import Utils.TestData;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.util.ReflectionUtils;

import java.io.IOException;
import java.util.*;

public class SequenceFileBinary_WriteAndReadWithMap_Test {

    private void writeSequenceFileWithMap(Configuration conf, DataItem[] data, Path outputFile, Path mapFile, boolean isAppend, boolean isLog) throws IOException {
        Map<String, Long> nav = new HashMap<>();

        Text myKey = new Text();
        BytesWritable myValue = new BytesWritable();

        SequenceFile.Writer writer = SequenceFile.createWriter(
            conf,
            SequenceFile.Writer.file(outputFile),
            SequenceFile.Writer.keyClass(myKey.getClass()),
            SequenceFile.Writer.valueClass(myValue.getClass()),
            SequenceFile.Writer.appendIfExists(isAppend));

        System.out.printf("Write to hadoop data, length: %s\n",data.length);

        try {
            for (DataItem item: data) {
                if (isLog) System.out.printf("[write key: %s]\n", item.key);

                myKey.set(item.key);
                myValue.set(new BytesWritable(item.data));
                nav.put(item.key, writer.getLength());
                writer.append(myKey, myValue);
            }
        } finally {
            IOUtils.closeStream(writer);
        }

        writeMapFile(conf, nav, mapFile, isAppend);
    }

    private String convertMapToString(Map<String, Long> nav) {
        StringBuilder str = new StringBuilder();
        nav.forEach((key, position) -> {
            str.append(key+"="+position+"\n");
        });
        return str.toString();
    }

    private Map<String, Long> convertMapFromString(String str) {
        Map<String, Long> nav = new HashMap<>();
        String[] array = str.split("\n", -1);
        for (String item: array) {
            int pos = item.indexOf("=");
            if (pos >= 0) {
                String key = item.substring(0, pos);
                Long position = Long.parseLong(item.substring(pos+1));
                nav.put(key, position);
            }
        }
        return nav;
    }

    private void writeMapFile(Configuration conf, Map<String, Long> nav, Path mapFile, boolean isAppend) throws IOException {
        FileSystem fs = FileSystem.get(conf);
        byte[] data = convertMapToString(nav).getBytes();
        Utils.HadoopFiles.writeToSimpleFile(fs, mapFile, data, isAppend);
    }

    private Map<String, Long> readMapFile(Configuration conf, Path mapFile) throws IOException {
        FileSystem fs = FileSystem.get(conf);
        byte[] data = Utils.HadoopFiles.readFromSimpleFile(fs, mapFile);
        String str = new String(data);
        return convertMapFromString(str);
    }

    private void readItemByKey(SequenceFile.Reader reader, Map<String, Long> nav, String key, Writable myKey, Writable myValue, boolean isLog) throws IOException {
        Long position = nav.get(key);
        reader.seek(position);
        reader.next(myKey, myValue);

        if (myKey!=null && key.equals(myKey.toString())) {
            if (isLog) System.out.printf("[srcKey=%s fileKey=%s position=%s]\n", key, myKey, position);
        } else {
            System.out.printf("ERROR [srcKey=%s fileKey=%s position=%s]\n", key, myKey, position);
        }
    }

    private void readSequenceFileWithMapDumb(Configuration conf, Path outputFile, Map<String, Long> nav, boolean isLog) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(
            conf,
            SequenceFile.Reader.file(outputFile)
        );

        try {
            Writable myKey = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
            Writable myValue = (Writable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

            System.out.println("Reading file WITH map as is");
            List<String> keys = new ArrayList<String>(nav.keySet());
            for (String key: keys) {
                readItemByKey(reader, nav, key, myKey, myValue, isLog);
            }

            System.out.println("Reading file WITH map shuffled");
            Collections.shuffle(keys);
            for (String key: keys) {
                readItemByKey(reader, nav, key, myKey, myValue, isLog);
            }

        } finally {
            IOUtils.closeStream(reader);
        }

    }

    private void readSequenceFileWithoutMapDumb(Configuration conf, Path outputFile, DataItem[] fileData, boolean isLog) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(
            conf,
            SequenceFile.Reader.file(outputFile)
        );

        System.out.println("Reading file WITHOUT map as is");

        try {
            Text myKey = new Text();
            BytesWritable myValue = new BytesWritable();
            long position = reader.getPosition();

            for (DataItem item: fileData) {
                if (!reader.next(myKey, myValue)) System.out.println("ERROR reading data");
                if (isLog) System.out.printf("[srcKey=%s fileKey=%s position=%s]\n", item.key, myKey, position);

                if (!item.key.equals(myKey.toString())) System.out.println("ERROR key!!!");
                if (!Arrays.equals(item.data, myValue.copyBytes())) System.out.println("ERROR data!!!");

                position = reader.getPosition();
            }
        } finally {
            IOUtils.closeStream(reader);
        }
    }

    private byte[] readSequenceFileByKeyWithMap(Configuration conf, Path outputFile, String key, Map<String, Long> nav, boolean isLog) throws IOException {

        if (isLog) System.out.printf("Reading single value WITH map for [srcKey %s]\n", key);
        SequenceFile.Reader reader = new SequenceFile.Reader(
            conf,
            SequenceFile.Reader.file(outputFile)
        );

        Writable myKey = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
        BytesWritable myValue = (BytesWritable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

        try {
            readItemByKey(reader, nav, key, myKey, myValue, isLog);
        } finally {
            IOUtils.closeStream(reader);
        }

        return myValue.copyBytes();
    }

    private byte[] readSequenceFileByKeyWithoutMap(Configuration conf, Path outputFile, String key, boolean isLog) throws IOException {

        if (isLog) System.out.printf("Reading single value WITHOUT map for [srcKey %s]\n", key);
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(outputFile)
        );

        byte[] data = new byte[0];

        try {
            Text myKey = new Text();
            long position = reader.getPosition();

            while (reader.next(myKey)) {
                if (key.equals(myKey.toString())) {
                    BytesWritable myValue = new BytesWritable();
                    reader.getCurrentValue(myValue);
                    data = myValue.copyBytes();
                    if (isLog) System.out.printf("[srcKey=%s fileKey=%s position=%s]\n", key, myKey, position);
                    break;
                }
                position = reader.getPosition();
            }
        } finally {
            IOUtils.closeStream(reader);
        }
        return data;
    }

    void testRun(Configuration conf, DataItem[] fileData, Path outputFile, Path mapFile) throws IOException {
        boolean isLog = false;
        byte[] data;
        Date start, end;

        start = new Date();
        writeSequenceFileWithMap(conf, fileData, outputFile, mapFile, true, isLog);
        end = new Date();
        System.out.printf("from: %s\n", start.toInstant());
        System.out.printf("  to: %s\n", end.toInstant());
        System.out.printf("elapsed: %s\n", end.getTime()-start.getTime()+"\n");

        start = new Date();
        Map<String, Long> navData1 = readMapFile(conf, mapFile);
        readSequenceFileWithMapDumb(conf, outputFile, navData1, isLog);
        end = new Date();
        System.out.printf("from: %s\n", start.toInstant());
        System.out.printf("  to: %s\n", end.toInstant());
        System.out.printf("elapsed: %s\n", end.getTime()-start.getTime()+"\n");

        start = new Date();
        readSequenceFileWithoutMapDumb(conf, outputFile, fileData, isLog);
        end = new Date();
        System.out.printf("from: %s\n", start.toInstant());
        System.out.printf("  to: %s\n", end.toInstant());
        System.out.printf("elapsed: %s\n", end.getTime()-start.getTime()+"\n");

        System.out.println("Check read WITH map - ordered");
        start = new Date();
        for (DataItem item: fileData) {
            Map<String, Long> navData = readMapFile(conf, mapFile);
            data = readSequenceFileByKeyWithMap(conf, outputFile,item.key, navData, isLog);
            if (Arrays.equals(data, item.data)) {
                if (isLog) System.out.printf("OK [check key: %s]\n", item.key);
            } else {
                System.out.printf("ERROR [check key: %s]\n", item.key);
            }
        }
        end = new Date();
        System.out.printf("from: %s\n", start.toInstant());
        System.out.printf("  to: %s\n", end.toInstant());
        System.out.printf("elapsed: %s\n", end.getTime()-start.getTime()+"\n");

        /*
        System.out.println("Check read WITHOUT map - ordered");
        start = new Date();
        for (DataItem item: fileData) {
            data = readSequenceFileByKeyWithoutMap(conf, outputFile,item.key, isLog);
            if (Arrays.equals(data, item.data)) {
                if (isLog) System.out.printf("OK [check key: %s]\n", item.key);
            } else {
                System.out.printf("ERROR [check key: %s]\n", item.key);
            }
        }
        end = new Date();
        System.out.printf("from: %s\n", start.toInstant());
        System.out.printf("  to: %s\n", end.toInstant());
        System.out.printf("elapsed: %s\n", end.getTime()-start.getTime()+"\n");
        */

        List<DataItem> targetList = Arrays.asList(fileData);
        Collections.shuffle(targetList);

        System.out.println("Check read WITH map - shuffled");
        start = new Date();
        for (DataItem item: fileData) {
            Map<String, Long> navData = readMapFile(conf, mapFile);
            data = readSequenceFileByKeyWithMap(conf, outputFile,item.key, navData, isLog);
            if (Arrays.equals(data, item.data)) {
                if (isLog) System.out.printf("OK [check key: %s]\n", item.key);
            } else {
                System.out.printf("ERROR [check key: %s]\n", item.key);
            }
        }
        end = new Date();
        System.out.printf("from: %s\n", start.toInstant());
        System.out.printf("  to: %s\n", end.toInstant());
        System.out.printf("elapsed: %s\n", end.getTime()-start.getTime()+"\n");

        /*
        System.out.println("Check read WITHOUT map - shuffled");
        start = new Date();
        for (DataItem item: fileData) {
            data = readSequenceFileByKeyWithoutMap(conf, outputFile,item.key, isLog);
            if (Arrays.equals(data, item.data)) {
                if (isLog) System.out.printf("OK [check key: %s]\n", item.key);
            } else {
                System.out.printf("ERROR [check key: %s]\n", item.key);
            }
        }
        end = new Date();
        System.out.printf("from: %s\n", start.toInstant());
        System.out.printf("  to: %s\n", end.toInstant());
        System.out.printf("elapsed: %s\n", end.getTime()-start.getTime()+"\n");
        */

//        data = readSequenceFileByKeyWithMap(conf, outputFile,"id2", navData1);
//        data = readSequenceFileByKeyWithMap(conf, outputFile,"id5", navData1);
//
//        data = readSequenceFileByKeyWithoutMap(conf, outputFile,"id2");
//        data = readSequenceFileByKeyWithoutMap(conf, outputFile,"id5");
    }

    public static void main(String[] args) throws IOException {
        ConfigReader configReader = new ConfigReader();
        Configuration conf = new Configuration();
        conf.addResource(configReader.getFileFromResourceAsStream("hadoop-conf.xml"));
        conf.set("dfs.replication", "3");

        DataItem[] fileData = TestData.getData();

        Path outputFile = new Path("hdfs://localhost:8020/test/seqFileSeekHuge.seq");
        Path mapFile = new Path("hdfs://localhost:8020/test/seqFileSeekHuge.map");

        SequenceFileBinary_WriteAndReadWithMap_Test s = new SequenceFileBinary_WriteAndReadWithMap_Test();
        s.testRun(conf, fileData, outputFile, mapFile);
    }

}
