package WorkingWithSequenceFiles;

import Utils.ConfigReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.util.ReflectionUtils;

import java.io.IOException;
import java.util.*;

public class SequenceFileText_WriteAndReadWithMap {

    private static final String[] CONTENT = {
            "Alex Brown!",
            "Alex Brown!",
            "Walking on the street",
            "Alex Brown, Alex Brown ...",
            "Infinite pull-ups!"
    };

    Map<String, Long> writeSequenceFile(Configuration conf, Path outputFile) throws IOException {
        Map<String, Long> nav = new HashMap<>();
        Text myKey = new Text();
        Text myValue = new Text();

        SequenceFile.Writer writer = SequenceFile.createWriter(
                conf,
                SequenceFile.Writer.file(outputFile),
                SequenceFile.Writer.keyClass(myKey.getClass()),
                SequenceFile.Writer.valueClass(myValue.getClass()),
                SequenceFile.Writer.appendIfExists(false));

        try {

            int counter = 0;
            for (String text :
                    CONTENT) {
                counter++;
                myKey.set(Integer.toString(counter));
                myValue.set(text);
                nav.put(Integer.toString(counter), writer.getLength());
                writer.append(myKey, myValue);
            }

        } finally {
            IOUtils.closeStream(writer);
        }
        return nav;
    }

    void readSequenceFile(Configuration conf, Path outputFile, Map<String, Long> nav) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(outputFile)
        );

        try {
            Writable myKey = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
            Writable myValue = (Writable) ReflectionUtils.newInstance(reader.getValueClass(), conf);


            for (String key :
                    nav.keySet()) {
                Long position = nav.get(key);
                reader.seek(position);
                reader.next(myKey, myValue);

                System.out.printf("[srcKey %s fileKey %s value %s]\n", key, myKey, myValue);

            }

            // read in reverse order
            List<String> keys = new ArrayList<String>(nav.keySet());
            Collections.reverse(keys);

            for (String key :
                    keys) {
                Long position = nav.get(key);
                reader.seek(position);
                reader.next(myKey, myValue);

                System.out.printf("[srcKey %s fileKey %s value %s]\n", key, myKey, myValue);

            }


        } finally {
            IOUtils.closeStream(reader);
        }

    }

    void readSequenceFileByKey(Configuration conf, Path outputFile, String key, Map<String, Long> nav) throws IOException {

        System.out.printf("Reading single value for [srcKey %s]\n", key);
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(outputFile)
        );

        try {
            Writable myKey = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
            Writable myValue = (Writable) ReflectionUtils.newInstance(reader.getValueClass(), conf);


            Long position = nav.get(key);
            reader.seek(position);
            reader.next(myKey, myValue);

            System.out.printf("[srcKey %s fileKey %s value %s pos: %s]\n", key, myKey, myValue, position);


        } finally {
            IOUtils.closeStream(reader);
        }
    }

    void testRun(Configuration conf, Path outputFile) throws IOException {
        Map<String, Long> navData = this.writeSequenceFile(conf, outputFile);
        this.readSequenceFile(conf, outputFile, navData);


        this.readSequenceFileByKey(conf, outputFile,"5", navData);

    }

    public static void main(String[] args) throws IOException {
        ConfigReader configReader = new ConfigReader();
        Configuration conf = new Configuration();
        conf.addResource(configReader.getFileFromResourceAsStream("hadoop-conf.xml"));
        conf.set("dfs.replication", "1");

        Path outputFile = new Path("hdfs://localhost:8020/test/seqFileSeek.seq");
        SequenceFileText_WriteAndReadWithMap s = new SequenceFileText_WriteAndReadWithMap();
        s.testRun(conf, outputFile);
    }

}
