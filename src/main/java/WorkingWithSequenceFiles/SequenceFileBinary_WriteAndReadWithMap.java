package WorkingWithSequenceFiles;

import Utils.ConfigReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.util.ReflectionUtils;

import java.io.IOException;
import java.util.*;

public class SequenceFileBinary_WriteAndReadWithMap {

    Map<String, Long> writeSequenceFile(Configuration conf, Path outputFile) throws IOException {
        Map<String, Long> nav = new HashMap<>();
        Text myKey = new Text();
        BytesWritable myValue = new BytesWritable();

        SequenceFile.Writer writer = SequenceFile.createWriter(
                conf,
                SequenceFile.Writer.file(outputFile),
                SequenceFile.Writer.keyClass(myKey.getClass()),
                SequenceFile.Writer.valueClass(myValue.getClass()),
                SequenceFile.Writer.appendIfExists(false));

        try {

            for (int i=1; i<=2; i++) {
                byte[] data = Utils.LocalFiles.readData("src\\main\\resources\\test_to_hadoop"+i+".mp4");

                myKey.set(Integer.toString(i));
                myValue.set(new BytesWritable(data));
                nav.put(Integer.toString(i), writer.getLength());
                writer.append(myKey, myValue);
            }

        } finally {
            IOUtils.closeStream(writer);
        }
        return nav;
    }

    void readSequenceFile(Configuration conf, Path outputFile, Map<String, Long> nav) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(outputFile)
        );

        try {
            Writable myKey = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
            Writable myValue = (Writable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

            for (String key :
                    nav.keySet()) {
                Long position = nav.get(key);
                reader.seek(position);
                reader.next(myKey, myValue);

//                System.out.printf("[srcKey %s fileKey %s value %s]\n", key, myKey, myValue);
                System.out.printf("[srcKey %s fileKey %s]\n", key, myKey);
            }

            // read in reverse order
            List<String> keys = new ArrayList<String>(nav.keySet());
            Collections.reverse(keys);

            for (String key :
                    keys) {
                Long position = nav.get(key);
                reader.seek(position);
                reader.next(myKey, myValue);

//                System.out.printf("[srcKey %s fileKey %s value %s]\n", key, myKey, myValue);
                System.out.printf("[srcKey %s fileKey %s]\n", key, myKey);

            }

        } finally {
            IOUtils.closeStream(reader);
        }

    }

    void readSequenceFileByKey(Configuration conf, Path outputFile, String key, Map<String, Long> nav) throws IOException {

        System.out.printf("Reading single value for [srcKey %s]\n", key);
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(outputFile)
        );

        try {
            Writable myKey = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
            Writable myValue = (Writable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

            Long position = nav.get(key);
            reader.seek(position);
            reader.next(myKey, myValue);

//            System.out.printf("[srcKey %s fileKey %s value %s pos: %s]\n", key, myKey, myValue, position);
            System.out.printf("[srcKey %s fileKey %s pos: %s]\n", key, myKey, position);


        } finally {
            IOUtils.closeStream(reader);
        }
    }

    void testRun(Configuration conf, Path outputFile) throws IOException {
        Map<String, Long> navData = this.writeSequenceFile(conf, outputFile);
        this.readSequenceFile(conf, outputFile, navData);

        this.readSequenceFileByKey(conf, outputFile,"2", navData);
    }

    public static void main(String[] args) throws IOException {
        ConfigReader configReader = new ConfigReader();
        Configuration conf = new Configuration();
        conf.addResource(configReader.getFileFromResourceAsStream("hadoop-conf.xml"));
        conf.set("dfs.replication", "1");

        Path outputFile = new Path("hdfs://localhost:8020/test/seqFileSeekBin.seq");
        SequenceFileBinary_WriteAndReadWithMap s = new SequenceFileBinary_WriteAndReadWithMap();
        s.testRun(conf, outputFile);
    }

}
