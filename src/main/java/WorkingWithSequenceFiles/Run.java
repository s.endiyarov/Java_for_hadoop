package WorkingWithSequenceFiles;

import Utils.ConfigReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.util.ReflectionUtils;

import java.io.IOException;

public class Run {
    private static final String[] DATA = {
            "One, two, buckle my shoe",
            "Three, four, shut the door",
            "Five, six, pick up sticks",
            "Seven, eight, lay them straight",
            "Nine, ten, a big fat hen"
    };

    public static void listDirFiles(FileSystem fs, Path root) throws IOException {
        RemoteIterator<LocatedFileStatus> iterator = fs.listFiles(root, false);
        while (iterator.hasNext()) {
            LocatedFileStatus fileStatus = iterator.next();
            Path path = fileStatus.getPath();
            System.out.println(path);

            String owner = fileStatus.getOwner();
            String paths = fileStatus.getPath() + "";
            boolean file = fileStatus.isFile();
            Long length = fileStatus.getLen();
            String group = fileStatus.getGroup();
            String permission = fileStatus.getPermission() + "";
            int replication = fileStatus.getReplication();

            System.out.println("Replication :" + replication);
            System.out.println("Group   :" + group);
            System.out.println("Length  :" + length);
            System.out.println("Owner   :" + owner);
            System.out.println("File     :" + file);
            System.out.println("Permission  :" + permission);
            System.out.println("Path    :" + paths);

        }
    }

    public static void writeToSequenceText(Configuration conf, Path file, boolean isAppend) throws IOException {
        Text myKey = new Text();
        Text myValue = new Text();

        SequenceFile.Writer writer = SequenceFile.createWriter(
                conf,
                SequenceFile.Writer.file(file),
                SequenceFile.Writer.keyClass(myKey.getClass()),
                SequenceFile.Writer.valueClass(myValue.getClass()),
                SequenceFile.Writer.appendIfExists(isAppend));

        try {
            for (int i = 0; i < 100; i++) {
                myKey.set("q" + String.valueOf(100 - i));
                myValue.set(DATA[i % DATA.length]);
                System.out.printf("[%s]\t%s\t%s\n", writer.getLength(), myKey, myValue);
                writer.append(myKey, myValue);
            }
//            myKey.set("q1111");
//            myValue.set("fdlkgjfdkljd");
//            writer.append(myKey, myValue);
        } finally {
            IOUtils.closeStream(writer);
        }
    }

    public static void readFromSequenceText(Configuration conf, Path file) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(file)
        );

        try {
            Writable myKey = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
            Writable myValue = (Writable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

            long position = reader.getPosition();
            while (reader.next(myKey, myValue)) {
                String syncSeen = reader.syncSeen() ? "*" : "";
                System.out.printf("[%s%s]\t%s\t%s\n", position, syncSeen, myKey, myValue);
                position = reader.getPosition(); // beginning of next record
            }
        } finally {
            IOUtils.closeStream(reader);
        }
    }

    public static void writeToSequenceBinary(Configuration conf, Path file, String key, byte[] data, boolean isAppend) throws IOException {
        Text myKey = new Text(key);
        BytesWritable myValue = new BytesWritable(data);

        SequenceFile.Writer writer = SequenceFile.createWriter(
                conf,
                SequenceFile.Writer.file(file),
                SequenceFile.Writer.keyClass(myKey.getClass()),
                SequenceFile.Writer.valueClass(myValue.getClass()),
                SequenceFile.Writer.appendIfExists(isAppend));

        try {
            writer.append(myKey, myValue);
        } finally {
            IOUtils.closeStream(writer);
        }
    }

    public static void readFromSequenceBinary(Configuration conf, Path file) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(file)
        );

        try {
            Text myKey = new Text();
            BytesWritable myValue = new BytesWritable();

            long position = reader.getPosition();
            while (reader.next(myKey, myValue)) {
                String syncSeen = reader.syncSeen() ? "*" : "";
                System.out.printf("[%s%s]\t%s\n", position, syncSeen, myKey);
                Utils.LocalFiles.writeData("src\\main\\resources\\test_to_hadoop_" + myKey + ".mp4_", myValue.copyBytes());
                position = reader.getPosition(); // beginning of next record
            }
        } finally {
            IOUtils.closeStream(reader);
        }
    }

/*
    public static byte[] readFromSequenceBinaryByKeyDumbVariant(Configuration conf, Path file, String key) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(file)
        );
        byte[] data = new byte[0];

        try {
            Text myKey = new Text();
            BytesWritable myValue = new BytesWritable();

            while (reader.next(myKey, myValue)) {
                if (key.equals(myKey.toString())) {
                    data = myValue.copyBytes();
                    break;
                }
            }
        } finally {
            IOUtils.closeStream(reader);
        }
        return data;
    }
*/

    public static byte[] readFromSequenceBinaryByKey(Configuration conf, Path file, String key) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(
                conf,
                SequenceFile.Reader.file(file)
        );
        byte[] data = new byte[0];

        try {
            Text myKey = new Text();

            while (reader.next(myKey)) {
                if (key.equals(myKey.toString())) {
                    BytesWritable myValue = new BytesWritable();
                    reader.getCurrentValue(myValue);
                    data = myValue.copyBytes();
                    break;
                }
            }
        } finally {
            IOUtils.closeStream(reader);
        }
        return data;
    }

    public static void main(String[] args) throws IOException {
        ConfigReader configReader = new ConfigReader();

        Configuration conf = new Configuration();
//        conf.set("io.serializations", "org.apache.hadoop.io.serializer.JavaSerialization");
        conf.addResource(configReader.getFileFromResourceAsStream("hadoop-conf.xml"));
        conf.set("dfs.replication", "3");

        FileSystem fs = FileSystem.get(conf);

        Path root = new Path("hdfs://localhost:8020/");
        Path fileHadoopSeqText = new Path("hdfs://localhost:8020/test.txt");
        Path fileHadoopSimpleBin1 = new Path("hdfs://localhost:8020/test1.mp4");
        Path fileHadoopSimpleBin2 = new Path("hdfs://localhost:8020/test2.mp4");
        Path fileHadoopSeqBin = new Path("hdfs://localhost:8020/test.dat");

        listDirFiles(fs, root);

        // проверка записи Sequence File с текстовыми данными
        writeToSequenceText(conf, fileHadoopSeqText, false);
        writeToSequenceText(conf, fileHadoopSeqText, true);

        // проверка чтения Sequence File с текстовыми данными
        readFromSequenceText(conf, fileHadoopSeqText);

        // проверка upload простых файлов в хадуп
        byte[] dataLocal1 = Utils.LocalFiles.readData("src\\main\\resources\\test_to_hadoop1.mp4");
        byte[] dataLocal2 = Utils.LocalFiles.readData("src\\main\\resources\\test_to_hadoop2.mp4");
        Utils.HadoopFiles.writeToSimpleFile(fs, fileHadoopSimpleBin1, dataLocal1, false);
        Utils.HadoopFiles.writeToSimpleFile(fs, fileHadoopSimpleBin2, dataLocal2, false);

        // проверка download простых файлов из хадупа
        byte[] dataHadoop1 = Utils.HadoopFiles.readFromSimpleFile(fs, fileHadoopSimpleBin1);
        Utils.LocalFiles.writeData("src\\main\\resources\\test_to_hadoop1.mp4_", dataHadoop1);
        byte[] dataHadoop2 = Utils.HadoopFiles.readFromSimpleFile(fs, fileHadoopSimpleBin2);
        Utils.LocalFiles.writeData("src\\main\\resources\\test_to_hadoop2.mp4_", dataHadoop2);

        // проверка записи Sequence File с бинарными данными
        writeToSequenceBinary(conf, fileHadoopSeqBin, "q1", dataLocal1, false);
        writeToSequenceBinary(conf, fileHadoopSeqBin, "q2", dataLocal2, true);

        // проверка чтения Sequence File с бинарными данными
        readFromSequenceBinary(conf, fileHadoopSeqBin);

        // download Sequence File с бинарными данными по ключу
        byte[] dataHadoopSeq1 =  readFromSequenceBinaryByKey(conf, fileHadoopSeqBin, "q1");
        Utils.LocalFiles.writeData("src\\main\\resources\\test_to_hadoop_seq_q1.mp4_", dataHadoopSeq1);
        byte[] dataHadoopSeq2 =  readFromSequenceBinaryByKey(conf, fileHadoopSeqBin, "q2");
        Utils.LocalFiles.writeData("src\\main\\resources\\test_to_hadoop_seq_q2.mp4_", dataHadoopSeq2);

    }
}
