package WorkingWithMapFile;

import org.apache.hadoop.io.BinaryComparable;
import org.apache.hadoop.io.Text;


public class MapTextKey extends Text {

    public MapTextKey() {
    }

    public MapTextKey(String string) {
        super(string);
    }

    @Override
    public int compareTo(BinaryComparable other) {
        return 0;
    }

    @Override
    public int compareTo(byte[] other, int off, int len) {
        return 0;
    }
}
