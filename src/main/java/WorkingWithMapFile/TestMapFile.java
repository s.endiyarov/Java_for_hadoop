package WorkingWithMapFile;

import Utils.ConfigReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import java.io.IOException;

public class TestMapFile {

    private static void createMapFile(Configuration conf, Path outputFile, Boolean appendToFile) {
        try {

            MapTextKey txtKey = new MapTextKey();
            Text txtValue = new Text();
            MapFile.Writer writer = null;

            Text key1 = new Text("Key1");
            Text value1 = new Text("Value1");
            SequenceFile.Metadata metadata = new SequenceFile.Metadata();
            metadata.set(key1, value1);
            SequenceFile.Writer.Option metadataOption = SequenceFile.Writer.metadata(metadata);

            try {
                writer = new MapFile.Writer(conf, outputFile,
                        MapFile.Writer.keyClass(MapTextKey.class),
                        MapFile.Writer.valueClass(Text.class),
                        SequenceFile.Writer.appendIfExists(appendToFile),
                        metadataOption);

                writer.setIndexInterval(1);//Need this as the default is 128, and my data is just 9 records

                txtKey.set("uuid1");
                txtValue.set("Value1");

                writer.append(txtKey, txtValue);

                txtKey.set("uuid2");
                txtValue.set("uuid2Value");


                writer.append(txtKey, txtValue);

            } finally {
                IOUtils.closeStream(writer);
                System.out.println("Map file created successfully!!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readMapFileByKey(Configuration conf, Path file, String key) throws IOException {

        MapFile.Reader reader = null;
        MapTextKey txtKey = new MapTextKey(key);
        Text txtValue = new Text();

        try {
            try {
                reader = new MapFile.Reader(file, conf);
                reader.get(txtKey, txtValue);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } finally {
            if (reader != null)
                reader.close();
        }
        System.out.println("The key is " + txtKey.toString()
                + " and the value is " + txtValue.toString());
    }

    public static void iterateOverMapFile(Configuration conf, Path file) throws IOException {

        System.out.println("Start iteration over file ...");

        MapFile.Reader reader = null;
        MapTextKey txtKey = new MapTextKey();
        Text txtValue = new Text();

        try {
            try {
                reader = new MapFile.Reader(file, conf);
                while(reader.next(txtKey, txtValue)){
                    System.out.println("The key is " + txtKey.toString()
                            + " and the value is " + txtValue.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } finally {
            if (reader != null)
                reader.close();
        }

    }

    public static void main(String[] args) throws IOException {

        //NB: Перемещение файла MapTextKey приводит к невозможности чтения, так как у файла меняетс япакте

        ConfigReader configReader = new ConfigReader();
        Configuration conf = new Configuration();
        conf.addResource(configReader.getFileFromResourceAsStream("hadoop-conf.xml"));
        conf.set("dfs.replication", "1");

        Path outputFile = new Path("hdfs://localhost:8020/test/mapFile.seq");

        createMapFile(conf, outputFile, true);
        readMapFileByKey(conf, outputFile, "1");

        readMapFileByKey(conf, outputFile, "uui2");

        iterateOverMapFile(conf, outputFile);


    }
}
